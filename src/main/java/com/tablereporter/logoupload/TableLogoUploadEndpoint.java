/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tablereporter.logoupload;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.core.util.Base64;
import com.tablereporter.properties.AppConfig;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Grzesiek
 */
@Component
@Path("logoupload")
public class TableLogoUploadEndpoint {
    
    @Autowired
    private AppConfig appConfig;
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String logoUpload(String imageJson) {
        try {
            if (isEmpty(imageJson)) {
                return "NO DATA POSTED";
            }
            Logo logo = getLogo(imageJson);
            File logoFile = saveLogo(logo);
            return producelogoUrl(logoFile);
        } catch (IOException ex) {
            Logger.getLogger(TableLogoUploadEndpoint.class.getName()).log(Level.SEVERE, "Failed to save logo file.", ex);
            return "FAIL";
        }
    }

    private File saveLogo(Logo logo) throws IOException {
        byte[] imageBytes = Base64.decode(logo.getBase64EncodedLogo());
        File file = produceLogoFile(logo);
        FileUtils.writeByteArrayToFile(file, imageBytes, false);
        return file;
    }

    private String getEmailSuffix(String userEmail) {
        String[] parts = userEmail.split("@");
        return parts[0]+"_at_"+parts[1].split("\\.")[0];
    }

    private File produceLogoFile(Logo logo) {
        int i=1;
        String path = produceLogoFilePath(logo, 1);
        File file = new File(path);
        while(file.exists()){
            i++;
            file = new File(produceLogoFilePath(logo, i));
        }
        return file;
    }

    private Logo getLogo(String imageJson) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(imageJson, Logo.class);
    }

    private String produceLogoFilePath(Logo logo, int i) {
        return appConfig.getUserLogoBasePath()+ 
                File.separator+"user_logo_" + 
                getEmailSuffix(logo.getUserEmail()) + 
                RandomStringUtils.random(40, "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM") + 
                i +
                "." + 
                logo.getImageFormat();
    }

    private String producelogoUrl(File logoFile) {
        return appConfig.getBaseLogoUrl() + logoFile.getName();
    }
}
