/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tablereporter.logoupload;

import com.tablereporter.properties.AppConfig;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Grzesiek
 */
@Component
public class ApiKeys {
    
    @Autowired
    private AppConfig appConfig;
    
    private Set<String> apiKeys;
    
    public boolean keyExist(String apiKey){
        return apiKeys.contains(apiKey);
    }
    
    @PostConstruct
    private void init(){
        apiKeys = initApiKeys();
    }

    private Set<String> initApiKeys() {
        String[] apiKeys = appConfig.getApiKeys().split(",");
        return new HashSet<>(Arrays.asList(apiKeys));
    }
}
