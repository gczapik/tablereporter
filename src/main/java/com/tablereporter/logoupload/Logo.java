/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tablereporter.logoupload;

/**
 *
 * @author Grzesiek
 */
public class Logo {
    
    private String userEmail;
    private String base64EncodedLogo;
    private String imageFormat; //i.e.: jpg, jpeg, png, etc...

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getBase64EncodedLogo() {
        return base64EncodedLogo;
    }

    public void setBase64EncodedLogo(String base64EncodedLogo) {
        this.base64EncodedLogo = base64EncodedLogo;
    }

    public String getImageFormat() {
        return imageFormat;
    }

    public void setImageFormat(String imageFormat) {
        this.imageFormat = imageFormat;
    }
    
    
}
