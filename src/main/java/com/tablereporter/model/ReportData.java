/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tablereporter.model;

/**
 *
 * @author Grzesiek
 */
public class ReportData {
    
    private DataTable dataTable;
    private String reportTitle;
    private String logoImageBase64Encoded;

    public ReportData() {
    }

    public ReportData(DataTable dataTable, String reportTitle, String logoImageBase64Encoded) {
        this.dataTable = dataTable;
        this.reportTitle = reportTitle;
        this.logoImageBase64Encoded = logoImageBase64Encoded;
    }

    public DataTable getDataTable() {
        return dataTable;
    }

    public void setDataTable(DataTable dataTable) {
        this.dataTable = dataTable;
    }

    public String getReportTitle() {
        return reportTitle;
    }

    public void setReportTitle(String reportTitle) {
        this.reportTitle = reportTitle;
    }

    public String getLogoImageBase64Encoded() {
        return logoImageBase64Encoded;
    }

    public void setLogoImageBase64Encoded(String logoImageBase64Encoded) {
        this.logoImageBase64Encoded = logoImageBase64Encoded;
    }

    
}
