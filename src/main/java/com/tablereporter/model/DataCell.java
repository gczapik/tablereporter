/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tablereporter.model;

/**
 *
 * @author Grzesiek
 */
public class DataCell {
    
    private String value;

    public DataCell() {
    }

    public DataCell(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    
}
