/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tablereporter.model;

import java.util.List;

/**
 *
 * @author Grzesiek
 */
public class DataTable {
    
    private List<String> columnNames;
    private List<DataRow> dataRows;

    public DataTable() {
    }

    public DataTable(List<String> columnNames, List<DataRow> dataRows) {
        this.columnNames = columnNames;
        this.dataRows = dataRows;
    }

    public List<String> getColumnNames() {
        return columnNames;
    }

    public void setColumnNames(List<String> columnNames) {
        this.columnNames = columnNames;
    }

    public List<DataRow> getDataRows() {
        return dataRows;
    }

    public void setDataRows(List<DataRow> dataRows) {
        this.dataRows = dataRows;
    }

    
}
