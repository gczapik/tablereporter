/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tablereporter.model;

import java.util.List;

/**
 *
 * @author Grzesiek
 */
public class DataRow {
    
    private String rowName;
    private List<DataCell> cells;

    public DataRow() {
    }

    public DataRow(String rowName, List<DataCell> cells) {
        this.rowName = rowName;
        this.cells = cells;
    }

    public String getRowName() {
        return rowName;
    }

    public void setRowName(String rowName) {
        this.rowName = rowName;
    }

    public List<DataCell> getCells() {
        return cells;
    }

    public void setCells(List<DataCell> cells) {
        this.cells = cells;
    }

    
}
