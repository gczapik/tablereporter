package com.tablereporter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tablereporter.generator.ReportGenerator;
import com.tablereporter.model.ReportData;
import java.io.InputStream;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Root resource (exposed at "myresource" path)
 */
@Component
@Path("generatereport")
public class GenerateReport {
    
    @Autowired
    private ReportGenerator reportGenerator;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getIt(String reportDataJson) throws Exception {
        ObjectMapper jsonMapper = new ObjectMapper();
        ReportData reportData = jsonMapper.readValue(reportDataJson, ReportData.class);
        return Response.ok(reportGenerator.generateReport(reportData), MediaType.APPLICATION_OCTET_STREAM)
                .build();
    }
}
