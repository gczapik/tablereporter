/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tablereporter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tablereporter.generator.ReportFileSaver;
import com.tablereporter.generator.ReportGenerator;
import com.tablereporter.generator.ReportUrlProducer;
import com.tablereporter.model.ReportData;
import java.io.File;
import java.io.InputStream;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Grzesiek
 */
@Component
@Path("generateandexpose")
public class GenerateReportAndExposeUrl {
    
    @Autowired
    private ReportGenerator reportGenerator;
    
    @Autowired
    private ReportFileSaver reportFileSaver;
    
    @Autowired
    private ReportUrlProducer reportUrlProducer;
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String saveAndGetUrl(String reportDataJson) throws Exception {
        ObjectMapper jsonMapper = new ObjectMapper();
        ReportData reportData = jsonMapper.readValue(reportDataJson, ReportData.class);
        InputStream report = reportGenerator.generateReport(reportData);
        File reportFile = reportFileSaver.saveGeneratedReportFile(report);
        String urlToReport = reportUrlProducer.produceUrlFor(reportFile);
        return urlToReport;
    }
}
