/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tablereporter.generator;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Component;

/**
 *
 * @author Grzesiek
 */
@Component
public class ImageDecoder {
    
    public byte[] decodeBase64(String imageString) throws IOException{
        Base64 base = new Base64(false);
        return base.decode(imageString);
    }
    
}
