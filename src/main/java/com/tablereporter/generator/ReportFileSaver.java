/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tablereporter.generator;


import com.tablereporter.properties.AppConfig;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import static java.nio.file.Files.copy;
import static java.nio.file.Files.copy;
import static java.nio.file.Files.copy;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Grzesiek
 */
@Component
public class ReportFileSaver {
    
    @Autowired
    private AppConfig appConfig;
    
    public File saveGeneratedReportFile(InputStream generatedPdf) {
        try {
            File reportFile = new File(appConfig.getBasePathForReportFiles()
                    + File.separator
                    + monthPlusRandomHash());
            reportFile.mkdirs();
            copy(
                    generatedPdf,
                    reportFile.toPath(),
                    StandardCopyOption.REPLACE_EXISTING);
            return reportFile;
        } catch (IOException ex) {
            Logger.getLogger(ReportFileSaver.class.getName()).log(Level.SEVERE, "Error during saving generated report to file system. Caused by: "+ ex.getMessage(), ex);
            return null;
        }
    }

    private String monthPlusRandomHash() {
        Date date = new Date();
        SimpleDateFormat month = new SimpleDateFormat("yyyy-MMM");
        return month.format(date) + "_" + String.valueOf(date.getTime()) + RandomStringUtils.random(40, "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM") + ".pdf";
    }
    
}
