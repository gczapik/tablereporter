/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tablereporter.generator;

import com.tablereporter.properties.AppConfig;
import java.io.File;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Grzesiek
 */
@Component
public class ReportUrlProducer {
    
    @Autowired
    private AppConfig appConfig;

    public String produceUrlFor(File reportFile) {
        String baseUrl = appConfig.getBaseUrlForReports();
        return baseUrl+reportFile.getName();
    }
    
}
