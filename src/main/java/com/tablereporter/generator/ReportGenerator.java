/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tablereporter.generator;

import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import com.tablereporter.model.DataCell;
import com.tablereporter.model.DataRow;
import com.tablereporter.model.DataTable;
import com.tablereporter.model.ReportData;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import static org.springframework.util.StringUtils.isEmpty;

/**
 *
 * @author Grzesiek
 */
@Component
public class ReportGenerator {
    public static final String DEST = "./simple_table.pdf";
    private static final String IMAGE_PATH = "./logo2.jpg";
 
    @Autowired
    private ImageDecoder imageDecoder;
    
    public static void main(String[] args) throws Exception {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        new ReportGenerator().generateReport(null);
    }
 

    public InputStream generateReport(ReportData reportData) throws Exception {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        PdfDocument pdfDoc = new PdfDocument(new PdfWriter(outStream));
        Document doc = new Document(pdfDoc);
 
        //----IMAGE ADDING-----
        Image logo = new Image(ImageDataFactory.create(imageDecoder.decodeBase64(reportData.getLogoImageBase64Encoded())));
        doc.add(logo.setWidthPercent(10));
        //---------------------
        
        //-------DOCUMENT HEADER------
        doc.setTextAlignment(TextAlignment.CENTER);
        doc.add(new Paragraph(""));
        doc.add(new Paragraph(""));
        doc.add(new Paragraph(""));
        doc.add(new Paragraph(reportData.getReportTitle()));
        doc.add(new Paragraph(""));
        doc.add(new Paragraph(""));
        //----------------------------
        
        DataTable dataTable = reportData.getDataTable();
        Table table = new Table(reportData.getDataTable().getColumnNames().size());
        table.setFontSize(4);
        Color backgroundColor = Color.CYAN;
        for(String columnHeader: dataTable.getColumnNames()){
            if(isEmpty(columnHeader)){
                table.addHeaderCell(createCell("", backgroundColor, 10f));
            } else{
                table.addHeaderCell(createCell(columnHeader, backgroundColor));
            }
        }
        backgroundColor = Color.WHITE;
        for (DataRow row: dataTable.getDataRows()) {
            table.addCell(createCell(row.getRowName(), backgroundColor));
            for(DataCell cell: row.getCells()){
                table.addCell(createCell(cell.getValue(), backgroundColor));
            }
            if (backgroundColor == Color.WHITE) {
                backgroundColor = Color.LIGHT_GRAY;
            } else {
                backgroundColor = Color.WHITE;
            }
        }
        doc.add(table);
        doc.close();
        outStream.flush(); //is this needed?
        return new ByteArrayInputStream(outStream.toByteArray());
    }

    private Cell createCell(String value, Color backgroundColor, Float percentageWidth) throws IOException {
        Cell cell = new Cell();
        if(percentageWidth != null){
            cell.setWidth(UnitValue.createPercentValue(percentageWidth));
        }
        cell.setBackgroundColor(backgroundColor);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.add(value);
        cell.setKeepTogether(true);
        return cell;
    }
    
    private Cell createCell(String value, Color backgroundColor) throws IOException {
        return createCell(value, backgroundColor, null);
    }
}
