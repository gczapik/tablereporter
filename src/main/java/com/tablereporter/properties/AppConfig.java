/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tablereporter.properties;

import java.io.IOException;
import java.util.Properties;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Grzesiek
 */
@Component
public class AppConfig {
    
    Properties props;

    @Autowired
    private PropertiesLoader propertiesLoader;
    
    @PostConstruct
    public void init() throws IOException {
        props = propertiesLoader.load("appconfig.properties");
    }
    
    public String getBasePathForReportFiles() {
        return props.getProperty("paths.reports");
    }

    public String getBaseUrlForReports() {
        return props.getProperty("urls.reports");
    }

    public String getUserLogoBasePath() {
        return props.getProperty("paths.logos");
    }

    public String getApiKeys() {
        return props.getProperty("app.apikeys");
    }

    public String getBaseLogoUrl() {
        return props.getProperty("images.base-url");
    }
    
}
